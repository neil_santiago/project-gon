﻿Public Class frmAdd
    Public addbal As Double
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        addbal = txtAddBal.Text
        Me.Hide()
    End Sub

    Private Sub txtAddBal_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtAddBal.KeyPress
        If (e.KeyChar.ToString = ".") And (txtAddBal.Text.Contains(e.KeyChar.ToString)) Then
            e.Handled = True
            Exit Sub
        End If
        If e.KeyChar <> ControlChars.Back Then
            e.Handled = Not (Char.IsDigit(e.KeyChar) Or e.KeyChar = "." Or e.KeyChar = "-")
        End If
        If e.KeyChar = vbCr Then
            addbal = txtAddBal.Text
            Me.Hide()
        End If
    End Sub

End Class