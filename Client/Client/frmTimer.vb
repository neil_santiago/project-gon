﻿Imports System.Data.OleDb

Public Class frmTimer
    Dim connectionString As String = "Provider=Microsoft.ACE.OLEDB.12.0; DATA SOURCE = C:\Accounts.accdb"

    Dim bal As Double
    Public time As Double

    Private Function performQuery(ByVal connectionString As String, ByVal sqlCommand As String) As OleDb.OleDbDataReader
        Dim dbConnection As OleDbConnection
        Dim dbCommand As New OleDbCommand()
        Dim dbDataReader As OleDb.OleDbDataReader = Nothing

        Try
            dbConnection = New OleDbConnection(connectionString)
            dbCommand.CommandText = sqlCommand
            dbCommand.Connection = dbConnection
            dbConnection.Open()
            dbDataReader = dbCommand.ExecuteReader
            Return dbDataReader
            dbConnection.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try

        Return Nothing
    End Function

    Private Function performNonQuery(ByVal connectionString As String, ByVal sqlCommand As String)
        Dim dbConnection As OleDbConnection
        Dim dbCommand As New OleDbCommand()

        Try
            dbConnection = New OleDbConnection(connectionString)
            dbCommand.CommandText = sqlCommand
            dbCommand.Connection = dbConnection
            dbCommand.Connection.Open()
            dbCommand.ExecuteNonQuery()
            dbConnection.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Return False
        End Try

        Return True
    End Function

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles tmrTimer.Tick
        If time > 0 Then
            time -= 1
            lblHrs.Text = time \ 60 \ 60
            lblMins.Text = time \ 60 Mod 60
            lblSecs.Text = time Mod 60 Mod 60
        ElseIf time < 1 Then
            tmrTimer.Stop()
            Dim dbDataReader As OleDb.OleDbDataReader = Nothing
            Dim sqlCommand As String = "UPDATE User_Accounts SET Balance = '" & time & "' WHERE User_Name = '" & frmLogIn.txtUserName.Text & "'"
            performNonQuery(connectionString, sqlCommand)
            MsgBox("Time's up")
            frmLogIn.Show()
            Me.Close()
            frmLogIn.txtPassword.Clear()
            frmLogIn.txtUserName.Clear()
        ElseIf time = 300 Then
            MsgBox("You have less than 5 minutes left.")
        End If
    End Sub

    Private Sub frmTimer_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        e.Cancel = True
    End Sub

    Private Sub frmTimer_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.ShowInTaskbar = False
        Dim dbDataReader As OleDb.OleDbDataReader = Nothing
        Dim sqlCommand1 As String = "SELECT * FROM User_Accounts WHERE User_Name = '" & frmLogIn.txtUserName.Text & "' AND User_Pass = '" & frmLogIn.txtPassword.Text & "'"
        dbDataReader = performQuery(connectionString, sqlCommand1)
        While dbDataReader.Read
            bal = dbDataReader("Balance".ToString)
            time = bal * 240
        End While

    End Sub

    Private Sub btnLOut_Click(sender As Object, e As EventArgs) Handles btnLOut.Click
        Dim dbDataReader As OleDb.OleDbDataReader = Nothing
        Dim sqlCommand As String = "UPDATE User_Accounts SET Balance = '" & time \ 240 & "' WHERE User_Name = '" & frmLogIn.txtUserName.Text & "'"
        performNonQuery(connectionString, sqlCommand)
        frmLogIn.Show()
        Me.Close()
        frmLogIn.txtUserName.Clear()
        frmLogIn.txtPassword.Clear()
    End Sub

    Private Sub lblColon1_Click(sender As Object, e As EventArgs) Handles lblColon1.Click

    End Sub

    Private Sub lblHrs_Click(sender As Object, e As EventArgs) Handles lblHrs.Click

    End Sub

    Private Sub grpTime_Enter(sender As Object, e As EventArgs) Handles grpTime.Enter

    End Sub
End Class