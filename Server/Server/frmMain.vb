﻿Imports System.Data.OleDb

Public Class frmMain
    Dim connectionString As String = "Provider=Microsoft.ACE.OLEDB.12.0; DATA SOURCE = C:\Accounts.accdb"

    Dim curbal As Double
    Dim newbal As Double

    Private Function performQuery(ByVal connectionString As String, ByVal sqlCommand As String) As OleDb.OleDbDataReader
        Dim dbConnection As OleDbConnection
        Dim dbCommand As New OleDbCommand()
        Dim dbDataReader As OleDb.OleDbDataReader = Nothing

        Try
            dbConnection = New OleDbConnection(connectionString)
            dbCommand.CommandText = sqlCommand
            dbCommand.Connection = dbConnection
            dbConnection.Open()
            dbDataReader = dbCommand.ExecuteReader
            Return dbDataReader
            dbConnection.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try

        Return Nothing
    End Function

    Private Function performNonQuery(ByVal connectionString As String, ByVal sqlCommand As String) As Boolean
        Dim dbConnection As OleDbConnection
        Dim dbCommand As New OleDbCommand()

        Try
            dbConnection = New OleDbConnection(connectionString)
            dbCommand.CommandText = sqlCommand
            dbCommand.Connection = dbConnection
            dbCommand.Connection.Open()
            dbCommand.ExecuteNonQuery()
            dbConnection.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Return False
        End Try

        Return True
    End Function

    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txtPass1.PasswordChar = "*"
        txtPass2.PasswordChar = "*"
        grpUser.Enabled = False
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btnMakeAccount.Click

        If btnMakeAccount.Text = "Create Account" Then
            grpUser.Enabled = True
            txtUserName.Enabled = True
            clearFields()
            btnMakeAccount.Text = "Save"
            txtSearch.Clear()
        ElseIf btnMakeAccount.Text = "Save" Then
            btnMakeAccount.Text = "Create Account"
            If txtUserName.Text.Trim.Length = 0 Then
                MsgBox("Please enter a username.")
            ElseIf txtPass1.Text.Trim.Length = 0 Then
                MsgBox("Please enter a password.")
            ElseIf txtPass2.Text.Trim.Length = 0 Then
                MsgBox("Please repeat password.")
            ElseIf txtBalance.Text.Trim.Length = 0 Then
                MsgBox("Please enter user's balance.")
            ElseIf (txtPass1.Text = txtPass2.Text) Then

                    Dim dbDataReader As OleDb.OleDbDataReader = Nothing
                    Dim sqlcommand1 As String = "SELECT * FROM User_Accounts WHERE User_Name = '" & txtUserName.Text & "'"
                    dbDataReader = performQuery(connectionString, sqlcommand1)
                    If dbDataReader.HasRows = 0 Then
                        Dim sqlCommand As String = "INSERT INTO User_Accounts (User_Name, User_Pass, Balance) VALUES ('" & txtUserName.Text & "', '" & txtPass1.Text & "', '" & txtBalance.Text & "')"
                    If performNonQuery(connectionString, sqlCommand) Then
                        clearFields()
                        MsgBox("Account successfully created.")
                        grpUser.Enabled = False
                    End If
                Else
                    MsgBox("Username already exists.")
                End If
                Else
                    MsgBox("Passwords have to match")
                End If
        End If

    End Sub

    Private Sub txtSearch_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtSearch.KeyPress

        If e.KeyChar = vbCr Then
            Dim dbDataReader As OleDb.OleDbDataReader = Nothing
            Dim sqlCommand As String = "SELECT * FROM User_Accounts WHERE User_Name = '" & txtSearch.Text & "'"
            dbDataReader = performQuery(connectionString, sqlCommand)
            If dbDataReader.HasRows Then
                While dbDataReader.Read
                    txtUserName.Text = dbDataReader("User_Name".ToString)
                    txtPass1.Text = dbDataReader("User_Pass".ToString)
                    txtPass2.Text = dbDataReader("User_Pass".ToString)
                    txtBalance.Text = dbDataReader("Balance".ToString)
                    curbal = dbDataReader("Balance".ToString)
                End While
            ElseIf dbDataReader.HasRows = 0 Then
                MsgBox("User not found.")
            End If
        End If

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
        If txtUserName.TextLength = 0 Then
            MsgBox("No account present to edit.")
        ElseIf txtUserName.TextLength > 1 Then
            If btnEdit.Text = "Edit Account" Then
                grpUser.Enabled = True
                txtUserName.Enabled = False
                btnEdit.Text = "Save Changes"
            ElseIf btnEdit.Text = "Save Changes" Then
                If txtUserName.Text.Trim.Length = 0 Then
                    MsgBox("Please enter a username.")
                ElseIf txtPass1.Text.Trim.Length = 0 Then
                    MsgBox("Please enter a password.")
                ElseIf txtPass2.Text.Trim.Length = 0 Then
                    MsgBox("Please repeat password.")
                ElseIf txtBalance.Text.Trim.Length = 0 Then
                    MsgBox("Please enter user's balance.")
                ElseIf (txtPass1.Text = txtPass2.Text) Then

                    Dim dbDataReader As OleDb.OleDbDataReader = Nothing
                    Dim sqlCommand As String = "UPDATE User_Accounts SET User_Name = '" & txtUserName.Text & "', User_Pass = '" & txtPass1.Text & "', Balance = '" & txtBalance.Text & "' WHERE User_Name = '" & txtUserName.Text & "'"
                    If performNonQuery(connectionString, sqlCommand) Then
                        MsgBox("Account updated successfuly.")
                        clearFields()
                        btnEdit.Text = "Edit Account"
                        grpUser.Enabled = False
                    Else
                        MsgBox("Passwords have to match.")
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub btnBalance_Click(sender As Object, e As EventArgs) Handles btnBalance.Click
        If btnBalance.Text = "Add Balance" Then
            frmAdd.Show()
            btnBalance.Text = "Add"
        ElseIf btnBalance.Text = "Add" Then
            newbal = curbal + frmAdd.addbal
            Dim dbDataReader As OleDb.OleDbDataReader = Nothing
            Dim sqlCommand As String = "UPDATE User_Accounts SET  Balance = '" & newbal & "' WHERE User_Name = '" & txtUserName.Text & "'"
            If performNonQuery(connectionString, sqlCommand) Then
                MsgBox("Successfully added to balance.")
                txtBalance.Text = newbal
            End If
            btnBalance.Text = "Add Balance"
            frmAdd.txtAddBal.Clear()
        End If
    End Sub

    Private Function clearFields()
        txtSearch.Clear()
        txtUserName.Clear()
        txtPass1.Clear()
        txtPass2.Clear()
        txtBalance.Clear()
        Return 0
    End Function

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        clearFields()
        grpUser.Enabled = False
        btnMakeAccount.Text = "Create Account"
        btnEdit.Text = "Edit Account"
        btnBalance.Text = "Add Balance"
        btnDelete.Text = "Delete Account"
    End Sub

    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        If btnDelete.Text = "Delete Account" Then
            btnDelete.Text = "Confirm Delete"
            MsgBox("Are you sure you want to delete this account?")
        ElseIf btnDelete.Text = "Confirm Delete" Then
            Dim dbDataReader As OleDb.OleDbDataReader = Nothing
            Dim sqlCommand As String = "DELETE * FROM User_Accounts WHERE User_Name = '" & txtUserName.Text & "'"
            If performNonQuery(connectionString, sqlCommand) Then
                clearFields()
                MsgBox("Account has been successfully deleted.")
            End If
            btnDelete.Text = "Delete Account"
            End If
    End Sub

    Private Sub txtBalance_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtBalance.KeyPress
        If (e.KeyChar.ToString = ".") And (txtBalance.Text.Contains(e.KeyChar.ToString)) Then
            e.Handled = True
            Exit Sub
        End If
        If e.KeyChar <> ControlChars.Back Then
            e.Handled = Not (Char.IsDigit(e.KeyChar) Or e.KeyChar = ".")
        End If
    End Sub
End Class