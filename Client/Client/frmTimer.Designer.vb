﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTimer
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.tmrTimer = New System.Windows.Forms.Timer(Me.components)
        Me.btnLOut = New System.Windows.Forms.Button()
        Me.lblHrs = New System.Windows.Forms.Label()
        Me.lblMins = New System.Windows.Forms.Label()
        Me.lblColon1 = New System.Windows.Forms.Label()
        Me.lblColon2 = New System.Windows.Forms.Label()
        Me.lblSecs = New System.Windows.Forms.Label()
        Me.grpTime = New System.Windows.Forms.GroupBox()
        Me.grpTime.SuspendLayout()
        Me.SuspendLayout()
        '
        'tmrTimer
        '
        Me.tmrTimer.Enabled = True
        Me.tmrTimer.Interval = 1000
        '
        'btnLOut
        '
        Me.btnLOut.Location = New System.Drawing.Point(36, 46)
        Me.btnLOut.Name = "btnLOut"
        Me.btnLOut.Size = New System.Drawing.Size(75, 23)
        Me.btnLOut.TabIndex = 0
        Me.btnLOut.Text = "Log Out"
        Me.btnLOut.UseVisualStyleBackColor = True
        '
        'lblHrs
        '
        Me.lblHrs.AutoSize = True
        Me.lblHrs.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHrs.Location = New System.Drawing.Point(9, 19)
        Me.lblHrs.Name = "lblHrs"
        Me.lblHrs.Size = New System.Drawing.Size(30, 24)
        Me.lblHrs.TabIndex = 1
        Me.lblHrs.Text = "00"
        '
        'lblMins
        '
        Me.lblMins.AutoSize = True
        Me.lblMins.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMins.Location = New System.Drawing.Point(66, 19)
        Me.lblMins.Name = "lblMins"
        Me.lblMins.Size = New System.Drawing.Size(30, 24)
        Me.lblMins.TabIndex = 2
        Me.lblMins.Text = "00"
        '
        'lblColon1
        '
        Me.lblColon1.AutoSize = True
        Me.lblColon1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblColon1.Location = New System.Drawing.Point(45, 19)
        Me.lblColon1.Name = "lblColon1"
        Me.lblColon1.Size = New System.Drawing.Size(15, 24)
        Me.lblColon1.TabIndex = 3
        Me.lblColon1.Text = ":"
        '
        'lblColon2
        '
        Me.lblColon2.AutoSize = True
        Me.lblColon2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblColon2.Location = New System.Drawing.Point(102, 19)
        Me.lblColon2.Name = "lblColon2"
        Me.lblColon2.Size = New System.Drawing.Size(15, 24)
        Me.lblColon2.TabIndex = 4
        Me.lblColon2.Text = ":"
        '
        'lblSecs
        '
        Me.lblSecs.AutoSize = True
        Me.lblSecs.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSecs.Location = New System.Drawing.Point(123, 19)
        Me.lblSecs.Name = "lblSecs"
        Me.lblSecs.Size = New System.Drawing.Size(30, 24)
        Me.lblSecs.TabIndex = 5
        Me.lblSecs.Text = "00"
        '
        'grpTime
        '
        Me.grpTime.Controls.Add(Me.btnLOut)
        Me.grpTime.Controls.Add(Me.lblSecs)
        Me.grpTime.Controls.Add(Me.lblHrs)
        Me.grpTime.Controls.Add(Me.lblColon2)
        Me.grpTime.Controls.Add(Me.lblMins)
        Me.grpTime.Controls.Add(Me.lblColon1)
        Me.grpTime.Location = New System.Drawing.Point(12, 12)
        Me.grpTime.Name = "grpTime"
        Me.grpTime.Size = New System.Drawing.Size(156, 77)
        Me.grpTime.TabIndex = 7
        Me.grpTime.TabStop = False
        Me.grpTime.Text = "Remaining Time"
        '
        'frmTimer
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(180, 100)
        Me.ControlBox = False
        Me.Controls.Add(Me.grpTime)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.Name = "frmTimer"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.Text = "frmTimer"
        Me.grpTime.ResumeLayout(False)
        Me.grpTime.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents tmrTimer As System.Windows.Forms.Timer
    Friend WithEvents btnLOut As System.Windows.Forms.Button
    Friend WithEvents lblHrs As System.Windows.Forms.Label
    Friend WithEvents lblMins As System.Windows.Forms.Label
    Friend WithEvents lblColon1 As System.Windows.Forms.Label
    Friend WithEvents lblColon2 As System.Windows.Forms.Label
    Friend WithEvents lblSecs As System.Windows.Forms.Label
    Friend WithEvents grpTime As System.Windows.Forms.GroupBox
End Class
